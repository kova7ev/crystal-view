# Crystal View Framework #

![dashboard.png](https://bitbucket.org/repo/9kgbXE/images/695854612-dashboard.png)

Easy in use framework for fast build new control using powerful opportunity OpenGL ES.
Texture, transparent segments, in-build text render engine, animation in nearest future -- it's all what you need.


```
#!c#

public class Wheel : ColorComponent
    {
        protected override float[] GetVertexCoord()
        {
            return youVertexArray;
        }

        protected override short[] GetDrawingOrder()
        {
            return youDrawingOrder;
        }
    }

```


Feel free and ask me for any help.