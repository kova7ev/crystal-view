﻿using Android.App;
using Android.OS;
using crystal_view.View;

namespace crystal_view
{
    [Activity(Label = "crystal_view", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private BidView _view;

        public MainActivity()
        {
            Utils.ApplicationContext.Context = this;
            _view = new BidView(this);
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
        }
    }
}