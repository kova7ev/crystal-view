using crystal_view.Utils;
using Java.Lang;

namespace crystal_view.Components
{
    public class SimpleText : TextureComponent
    {
        public float PositionX
        {
            get { return _positionX; }
            set
            {
                _positionX = value;
                MarkAsDirt();
            }
        }

        public float PositionY
        {
            get { return _positionY; }
            set
            {
                _positionY = value;
                MarkAsDirt();
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                MarkAsDirt();
            }
        }

        public float FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                MarkAsDirt();
            }
        }

        public float[] Color
        {
            get { return _color; }
            set
            {
                _color = value; 
                MarkAsDirt();
            }
        }

        private float _positionX;
        private float _positionY;
        private string _text;
        private float _fontSize;
        private float[] _color;

        public SimpleText(float posX, float posY, string text, float fontSize, string color)
            : this(posX, posY, text, fontSize, ColorHelper.hexToColor(color))
        {
        }

        public SimpleText(float posX, float posY, string text, float fontSize, float[] color)
        {
            PositionX = posX;
            PositionY = posY;
            Text = text;
            FontSize = fontSize;
            Color = color;
        }

        /**
         * Encapsulates the OpenGL ES instructions for drawing this shape.
         *
         * @param mvpMatrix
         *            - The Model View Project matrix in which to draw this shape.
         */

        protected override float[] GetVertexCoord()
        {
            var numberCharacters = Text.Replace(" ", "").Length;

            var vertexCoords = new float[numberCharacters * 4 * 3];
            var vertexCoordOffset = 0;

            //float size = Math.Max(TextRenderer.Bitmap.Width, TextRenderer.Bitmap.Height);
			float size = (4096.0f * ScreenWidth / 768);//TextRenderer.Bitmap.Width * TextRenderer.Bitmap.Height;
            var avgCharacterWidthNormolized = 1.0f * TextRenderer.AvgCharacterWidth / size;
            var avgCharacterHeightNormolized = 1.0f * TextRenderer.AvgCharacterHeight / size;
            //var textWidthRough = FontSize * avgCharacterWidthNormolized * Text.Length;
            var textWidthAccurate = FontSize * TextRenderer.GetTextBoundingBox(Text).Right / size;

            var posX = PositionX + textWidthAccurate / 2;

            for (var j = 0; j < Text.Length; j++)
            {
                var c = Text.Substring(j, 1);
                var rect = TextRenderer.GetTextureCoords(c);

                if (rect == null)
                {
                    throw new RuntimeException();
                }

                if (c.Equals(" "))
                {
                    posX = posX - FontSize * avgCharacterWidthNormolized;
                    continue;
                }

                var convexHullWidth = rect.Right - rect.Left;
                var convexHullHeight = rect.Bottom - rect.Top;

                var width = 1.0f * convexHullWidth / size;
                var height = 1.0f * convexHullHeight / size;
				var dH = 0.5f * FontSize * (height - avgCharacterHeightNormolized);

                vertexCoords[vertexCoordOffset] = posX;
                vertexCoords[vertexCoordOffset + 1] = PositionY + FontSize * height / 2 + dH;
                vertexCoords[vertexCoordOffset + 2] = 0.0f;

                vertexCoords[vertexCoordOffset + 3] = posX;
                vertexCoords[vertexCoordOffset + 4] = PositionY - FontSize * height / 2 + dH;
                vertexCoords[vertexCoordOffset + 5] = 0.0f;

                vertexCoords[vertexCoordOffset + 6] = posX - FontSize * width;
                vertexCoords[vertexCoordOffset + 7] = PositionY - FontSize * height / 2 + dH;
                vertexCoords[vertexCoordOffset + 8] = 0.0f;

                vertexCoords[vertexCoordOffset + 9] = posX - FontSize * width;
                vertexCoords[vertexCoordOffset + 10] = PositionY + FontSize * height / 2 + dH;
                vertexCoords[vertexCoordOffset + 11] = 0.0f;

                vertexCoordOffset += 12;
                posX = posX - FontSize * width;
            }

            return vertexCoords;
        }

        protected override float[] GetTextureCoord()
        {
            var numberCharacters = Text.Replace(" ", "").Length;
            var textureCoords = new float[numberCharacters * 4 * 2];
            var index = 0;

            for (var j = 0; j < Text.Length; j++)
            {
                var c = Text.Substring(j, 1);
                var rect = TextRenderer.GetTextureCoords(c);

                if (rect == null)
                {
                    throw new RuntimeException();
                }

                if (c.Equals(" "))
                {
                    continue;
                }

                var left = 1.0f * rect.Left / TextRenderer.Bitmap.Width;
                var top = 1.0f * rect.Top / TextRenderer.Bitmap.Height;
                var right = 1.0f * rect.Right / TextRenderer.Bitmap.Width;
                var bottom = 1.0f * rect.Bottom / TextRenderer.Bitmap.Height;

                textureCoords[index] = left;
                textureCoords[index + 1] = top;
                textureCoords[index + 2] = left;
                textureCoords[index + 3] = bottom;
                textureCoords[index + 4] = right;
                textureCoords[index + 5] = bottom;
                textureCoords[index + 6] = right;
                textureCoords[index + 7] = top;

                index += 8;
            }

            return textureCoords;
        }

        protected override short[] GetDrawingOrder()
        {
            var numberCharacters = Text.Replace(" ", "").Length;
            var drawOrder = new short[numberCharacters * 6];
            var index = 0;

            for (var i = 0; i < numberCharacters; i++)
            {
                // { 0, 1, 2, 0, 2, 3 }
                drawOrder[index] = (short)(i * 4);
                drawOrder[index + 1] = (short)(i * 4 + 1);
                drawOrder[index + 2] = (short)(i * 4 + 2);
                drawOrder[index + 3] = (short)(i * 4);
                drawOrder[index + 4] = (short)(i * 4 + 2);
                drawOrder[index + 5] = (short)(i * 4 + 3);

                index += 6;
            }

            return drawOrder;
        }
    }
}