using crystal_view.Utils;

namespace crystal_view.Components
{
    public class Line : ColorComponent
    {
        public float X0
        {
            get { return _x0; }
            set
            {
                _x0 = value; 
                MarkAsDirt();
            }
        }

        public float Y0
        {
            get { return _y0; }
            set
            {
                _y0 = value; 
                MarkAsDirt();
            }
        }

        public float X1
        {
            get { return _x1; }
            set
            {
                _x1 = value; 
                MarkAsDirt();
            }
        }

        public float Y1
        {
            get { return _y1; }
            set
            {
                _y1 = value;
                MarkAsDirt();
            }
        }

        public float Width
        {
            get { return _width; }
            set
            {
                _width = value; 
                MarkAsDirt();
            }
        }

        private float _x0;
        private float _y0;
        private float _x1;
        private float _y1;
        private float _width;

        private static float _screeWidth;
        private static float _screeHeight;

        public Line(float x0, float y0, float x1, float y1, float width, string color) : this(x0, y0, x1, y1, width, ColorHelper.hexToColor(color))
        {
        }

        public Line(float x0, float y0, float x1, float y1, float width, float[] color)
        {
            X0 = x0;
            Y0 = y0;
            X1 = x1;
            Y1 = y1;
            Width = width;
            Color = color;
        }

        protected override float[] GetVertexCoord()
        {
            var width = Width / _screeWidth;

            var vertexCoords = new float[4 * 3];
            vertexCoords[0] = X0 - width / 2;
            vertexCoords[1] = Y0 - width / 2;
            vertexCoords[2] = 0.0f;

            vertexCoords[3] = X0 + width / 2;
            vertexCoords[4] = Y0 + width / 2;
            vertexCoords[5] = 0.0f;

            vertexCoords[6] = X1 + width / 2;
            vertexCoords[7] = Y1 + width / 2;
            vertexCoords[8] = 0.0f;

            vertexCoords[9] = X1 - width / 2;
            vertexCoords[10] = Y1 - width / 2;
            vertexCoords[11] = 0.0f;

            return vertexCoords;
        }

        protected override short[] GetDrawingOrder()
        {
            var drawOrder = new short[6];
            // { 0, 1, 2, 0, 2, 3 }
            drawOrder[0] = 0;
            drawOrder[1] = 1;
            drawOrder[2] = 2;
            drawOrder[3] = 0;
            drawOrder[4] = 2;
            drawOrder[5] = 3;

            return drawOrder;
        }

        public static void OnSurfaceChanged(int width, int height)
        {
            _screeWidth = width;
            _screeHeight = height;
        }
    }
}