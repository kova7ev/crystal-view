using Android.Opengl;
using crystal_view.Utils;
using Java.Nio;
using EGLConfig = Javax.Microedition.Khronos.Egl.EGLConfig;

namespace crystal_view.Components
{
    public class ColorComponent : Component
    {
        private int _colorHandle;
        protected float[] Color { get; set; }

        protected ColorComponent()
        {
            
        }

        public override void OnSurfaceCreated(EGLConfig config)
        {
            var vertexShader = GLUtil.loadShader(GLES20.GlVertexShader, Shaders.SimpleVertexShader);
            var fragmentShader = GLUtil.loadShader(GLES20.GlFragmentShader, Shaders.SimpleFragmentShader);

            Program = GLES20.GlCreateProgram(); // create empty OpenGL
            // Program
            GLES20.GlAttachShader(Program, vertexShader); // add the vertex
            // shader
            // to program
            GLES20.GlAttachShader(Program, fragmentShader); // add the
            // fragment
            // shader to
            // program
            GLES20.GlLinkProgram(Program); // create OpenGL program
        }

        public override void Draw(float[] mvpMatrix)
        {
			if (ModelChanged)
            {
                VertexCoords = null;
                DrawOrder = null;
                TextureCoords = null;
                ModelChanged = false;
            }

			if (VertexCoords.Length == 0) {
				return;
			}

            // Add program to OpenGL environment
            GLES20.GlUseProgram(Program);

            // initialize vertex byte buffer for shape coordinates
            var bb = ByteBuffer.AllocateDirect(
                // (number of coordinate values * 4 bytes per float)
                VertexCoords.Length*4);
            // use the device hardware's native byte order
            bb.Order(ByteOrder.NativeOrder());

            // create a floating point buffer from the ByteBuffer
            VertexBuffer = bb.AsFloatBuffer();
            // add the coordinates to the FloatBuffer
            VertexBuffer.Put(VertexCoords);
            // set the buffer to read the first coordinate
            VertexBuffer.Position(0);

            // initialize byte buffer for the draw list
            var dlb = ByteBuffer.AllocateDirect(
                // (# of coordinate values * 2 bytes per short)
                DrawOrder.Length*2);
            dlb.Order(ByteOrder.NativeOrder());
            DrawListBuffer = dlb.AsShortBuffer();
            DrawListBuffer.Put(DrawOrder);
            DrawListBuffer.Position(0);

            // get handle to vertex shader's vPosition member
            PositionHandle = GLES20.GlGetAttribLocation(Program,
                "vPosition");
			GLUtil.checkGlError("glGetAttribLocation");

            // Enable a handle to the triangle vertices
            GLES20.GlEnableVertexAttribArray(PositionHandle);
			GLUtil.checkGlError("glEnableVertexAttribArray");

            // Prepare the triangle coordinate data
            GLES20.GlVertexAttribPointer(PositionHandle, CoordsPerVertex,
                GLES20.GlFloat, false, VertexStride, VertexBuffer);
			GLUtil.checkGlError("glVertexAttribPointer");

            // get handle to fragment shader's vColor member
            _colorHandle = GLES20.GlGetUniformLocation(Program,
                "vColor");

            // Set color for drawing the triangle
            GLES20.GlUniform4fv(_colorHandle, 1, Color, 0);
			GLUtil.checkGlError("glUniform4fv");

            // get handle to shape's transformation matrix
            MvpMatrixHandle = GLES20.GlGetUniformLocation(Program,
                "uMVPMatrix");
            GLUtil.checkGlError("glGetUniformLocation");

            // Apply the projection and view transformation
            GLES20.GlUniformMatrix4fv(MvpMatrixHandle, 1, false, mvpMatrix,
                0);
            GLUtil.checkGlError("glUniformMatrix4fv");

            // Draw the square
            GLES20.GlDrawElements(GLES20.GlTriangles, DrawOrder.Length,
                GLES20.GlUnsignedShort, DrawListBuffer);

            // Disable vertex array
            GLES20.GlDisableVertexAttribArray(PositionHandle);
        }
    }
}