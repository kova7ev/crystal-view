using Android.Opengl;
using Android.OS;
using crystal_view.Utils;
using Java.Nio;
using EGLConfig = Javax.Microedition.Khronos.Egl.EGLConfig;

namespace crystal_view.Components
{
    public class TextureComponent : Component
    {
        private int _alphaHandle;
        private int _textureHandle;
        private int _textureCoordsHandle;
        private int[] _textureHandles;

        protected TextureComponent()
        {
        }

        public override void Draw(float[] mvpMatrix)
        {
			if (_textureHandles == null) {
				return;
			}

            if (ModelChanged)
            {
                VertexCoords = null;
                DrawOrder = null;
                TextureCoords = null;
                ModelChanged = false;
            }

            // Add program to OpenGL environment
            GLES20.GlUseProgram(Program);

            // initialize vertex byte buffer for shape coordinates
            var bb = ByteBuffer.AllocateDirect(
                // (# of coordinate values * 4 bytes per float)
                TextureCoords.Length*4).Order(ByteOrder.NativeOrder());

            TextureBuffer = bb.AsFloatBuffer();
            TextureBuffer.Put(TextureCoords);
            TextureBuffer.Position(0);

            // initialize vertex byte buffer for shape coordinates
            bb = ByteBuffer.AllocateDirect(
                // (# of coordinate values * 4 bytes per float)
                VertexCoords.Length*4);
            bb.Order(ByteOrder.NativeOrder());
            VertexBuffer = bb.AsFloatBuffer();
            VertexBuffer.Put(VertexCoords);
            VertexBuffer.Position(0);

            // initialize byte buffer for the draw list
            var dlb = ByteBuffer.AllocateDirect(
                // (# of coordinate values * 2 bytes per short)
                DrawOrder.Length*2);
            dlb.Order(ByteOrder.NativeOrder());
            DrawListBuffer = dlb.AsShortBuffer();
            DrawListBuffer.Put(DrawOrder);
            DrawListBuffer.Position(0);

            // get handle to vertex shader's vPosition member
            PositionHandle = GLES20.GlGetAttribLocation(Program,
                "aPosition");

            // Enable a handle to the triangle vertices
            GLES20.GlEnableVertexAttribArray(PositionHandle);

            // Prepare the triangle coordinate data
            GLES20.GlVertexAttribPointer(PositionHandle, CoordsPerVertex,
                GLES20.GlFloat, false, VertexStride, VertexBuffer);

            GLES20.GlBindTexture(GLES20.GlTexture2d, _textureHandles[0]);
            GLUtil.checkGlError("glBindTexture");

            _textureCoordsHandle = GLES20.GlGetAttribLocation(Program,
                "aTexCoords");
            GLUtil.checkGlError("glGetAttribLocation");

            GLES20.GlEnableVertexAttribArray(_textureCoordsHandle);
            GLUtil.checkGlError("glEnableVertexAttribArray");

            GLES20.GlVertexAttribPointer(_textureCoordsHandle,
                CoordsPerTextureVertex, GLES20.GlFloat, false,
                TextureVertexStrideBytes, TextureBuffer);
            GLUtil.checkGlError("glVertexAttribPointer");

            _textureHandle = GLES20.GlGetUniformLocation(Program,
                "uTexture");
            GLUtil.checkGlError("glGetUniformLocation");

            GLES20.GlUniform1i(_textureHandle, 0);
            GLUtil.checkGlError("glUniform1i");

            GLES20.GlActiveTexture(GLES20.GlTexture0);
            GLUtil.checkGlError("glActiveTexture");

            GLES20.GlBindTexture(GLES20.GlTexture2d, _textureHandles[0]);
            GLUtil.checkGlError("glBindTexture");
            // Set the alpha
            _alphaHandle = GLES20.GlGetUniformLocation(Program, "uAlpha");
            GLUtil.checkGlError("glGetUniformLocation");
            GLES20.GlUniform1f(_alphaHandle, 1.0f);
            GLUtil.checkGlError("glUniform1f");

            // get handle to shape's transformation matrix
            MvpMatrixHandle = GLES20.GlGetUniformLocation(Program,
                "uMVPMatrix");
            GLUtil.checkGlError("glGetUniformLocation");

            // Apply the projection and view transformation
            GLES20.GlUniformMatrix4fv(MvpMatrixHandle, 1, false, mvpMatrix, 0);
            GLUtil.checkGlError("glUniformMatrix4fv");

            // Draw the square
            GLES20.GlDrawElements(GLES20.GlTriangles, DrawOrder.Length,
                GLES20.GlUnsignedShort, DrawListBuffer);

            // Disable vertex array
            GLES20.GlDisableVertexAttribArray(PositionHandle);

            //GLES20.GlDeleteTextures(_textureHandles.Length, _textureHandles, 0);
            //GLUtil.checkGlError("glDeleteTextures");
            //_textureHandles = null;
        }

        public override void OnSurfaceCreated(EGLConfig config)
        {
			var vertexShader = GLUtil.loadShader(GLES20.GlVertexShader, Shaders.VertexShader);
			var fragmentShader = GLUtil.loadShader(GLES20.GlFragmentShader, Shaders.FragmentShader);

			Program = GLES20.GlCreateProgram(); // create empty OpenGL
			// Program
			GLES20.GlAttachShader(Program, vertexShader); // add the vertex
			// shader
			// to program
			GLES20.GlAttachShader(Program, fragmentShader); // add the
			// fragment
			// shader to
			// program
			GLES20.GlLinkProgram(Program); // create OpenGL program

			while (TextRenderer.Bitmap == null) {
				SystemClock.Sleep (300);
			}

            //Load bitmap as texture
            if (_textureHandles == null)
            {
                _textureHandles = new int[1];
                _textureHandles[0] = GLUtil.LoadTexture(TextRenderer.Bitmap);
            }
        }

        //public void OnSurfaceCreated(EGLConfig config)
        //{
//            await TextRenderer._semaphore.WaitAsync();
//
//            try
//            {
//				if (Bitmap == null || TextureHandles == null)
//                {
//                    var bitmap = TextRenderer.Bitmap;
//                    TextureHandles = new int[1];
//                    TextureHandles[0] = GLUtil.LoadTexture(bitmap);
//                    Bitmap = bitmap;
//                }
//            }
//            finally
//            {
//                TextRenderer._semaphore.Release();
//            }

			//if (Bitmap == null || TextureHandles == null)
			//{
			//	var bitmap = TextRenderer.Bitmap;
			//	TextureHandles = new int[1];
			//	TextureHandles[0] = GLUtil.LoadTexture(bitmap);
			//	Bitmap = bitmap;
			//}
        //}
    }
}