namespace crystal_view.Components
{
    public class Text
    {
        private string mText = "OpenGL text";
        private float mPosX;
        private float mPosY;

        public Text(float posX, float posY, string text)
        {
            mPosX = posX;
            mPosY = posY;
            mText = text;
        }

        public float getPositionX()
        {
            return mPosX;
        }

        public float getPositionY()
        {
            return mPosY;
        }

        public string getText()
        {
            return mText;
        }
    }
}