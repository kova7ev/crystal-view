using System;
using crystal_view.Utils;
using Java.Nio;
using Javax.Microedition.Khronos.Egl;
using Javax.Microedition.Khronos.Opengles;

namespace crystal_view.Components
{
    public abstract class Component
    {
        protected FloatBuffer VertexBuffer;
        protected ShortBuffer DrawListBuffer;
        protected FloatBuffer TextureBuffer;

        protected int Program;

        private float[] _vertexCoords;
        private float[] _textureCoords;
        private short[] _drawOrder;

        protected int MvpMatrixHandle;
        protected int PositionHandle;

        protected static readonly int CoordsPerVertex = 3;
        protected static readonly int VertexStride = CoordsPerVertex*4; // 4 bytes per vertex S, T (or X, Y)
        protected static readonly int CoordsPerTextureVertex = 2;
        protected static readonly int TextureVertexStrideBytes = CoordsPerTextureVertex*GLUtil.BYTES_PER_FLOAT;

        protected bool ModelChanged { get; set; }

        public int ScreenWidth;
        public int ScreenHeight;

        protected float[] VertexCoords
        {
            get
            {
                if (_vertexCoords == null)
                {
                    _vertexCoords = GetVertexCoord();
                }
                return _vertexCoords;
            }
            set { _vertexCoords = value; }
        }

        protected float[] TextureCoords
        {
            get
            {
                if (_textureCoords == null)
                {
                    _textureCoords = GetTextureCoord();
                }
                return _textureCoords;
            }
            set { _textureCoords = value; }
        }

        protected short[] DrawOrder
        {
            get
            {
                if (_drawOrder == null)
                {
                    _drawOrder = GetDrawingOrder();
                }
                return _drawOrder;
            }
            set { _drawOrder = value; }
        }

        public virtual void Draw(float[] mvpMatrix)
        {
            throw new NotImplementedException();
        }

        protected virtual float[] GetVertexCoord()
        {
            throw new NotImplementedException();
        }

        protected virtual float[] GetTextureCoord()
        {
            throw new NotImplementedException();
        }

        protected virtual short[] GetDrawingOrder()
        {
            throw new NotImplementedException();
        }

        protected void MarkAsDirt()
        {
            ModelChanged = true;
        }

        public virtual void OnSurfaceCreated(EGLConfig config)
        {
            
        }

        public virtual void OnSurfaceChanged(IGL10 gl, int width, int height)
        {
            ScreenWidth = width;
            ScreenHeight = height;
        }
    }
}