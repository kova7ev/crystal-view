using System;
using crystal_view.Utils;

namespace crystal_view.Components
{
    public class CircleSegment : ColorComponent
    {
        private float _x;
        private float _y;
        private float _startAngle;
        private float _endAngle;
        private float _dxAngle;
        private float _radius;
        private float _dxRadius;

        public float X
        {
            get { return _x; }
            set
            {
                _x = value;
                MarkAsDirt();
            }
        }

        public float Y
        {
            get { return _y; }
            set
            {
                _y = value;
                MarkAsDirt();
            }
        }

        public float StartAngle
        {
            get { return _startAngle; }
            set
            {
                _startAngle = value + 90; 
                MarkAsDirt();
            }
        }

        public float EndAngle
        {
            get { return _endAngle; }
            set
            {
                _endAngle = value + 90;
                MarkAsDirt();
            }
        }

        public float DxAngle
        {
            get { return _dxAngle; }
            set
            {
                _dxAngle = value; 
                MarkAsDirt();
            }
        }

        public float Radius
        {
            get { return _radius; }
            set
            {
                _radius = value;
                MarkAsDirt();
            }
        }

        public float DxRadius
        {
            get { return _dxRadius; }
            set
            {
                _dxRadius = value;
                MarkAsDirt();
            }
        }

        public CircleSegment(float x, float y, float startAngle, float endAngle, float radius, float dxRadius,
            string color)
            : this(x, y, startAngle, endAngle, radius, dxRadius, ColorHelper.hexToColor(color))
        {
        }

        public CircleSegment(float x, float y, float startAngle, float endAngle, float radius, float dxRadius,
            float[] color)
        {
            X = x;
            Y = y;
            StartAngle = startAngle;
            EndAngle = endAngle;
            Radius = radius;
            DxAngle = 1;
            DxRadius = dxRadius;
            Color = color;
        }

        protected override float[] GetVertexCoord()
        {
            var numberRect = (int) ((EndAngle - StartAngle)/DxAngle);
            var vertexCoords = new float[numberRect*4*3];
            var index = 0;

            for (var i = 0; i < numberRect; i++)
            {
                //V0
                var angle = StartAngle + DxAngle*i;
                var y = Y + (float) (Radius*Math.Sin(MathHelper.degreeToRad(angle)));
                var x = X + (float) (Radius*Math.Cos(MathHelper.degreeToRad(angle)));

                vertexCoords[index] = x;
                vertexCoords[index + 1] = y;
                vertexCoords[index + 2] = 0.0f;

                index += 3;

                //V1
                y = Y + (float) (Radius*Math.Sin(MathHelper.degreeToRad(angle + DxAngle)));
                x = X + (float) (Radius*Math.Cos(MathHelper.degreeToRad(angle + DxAngle)));

                vertexCoords[index] = x;
                vertexCoords[index + 1] = y;
                vertexCoords[index + 2] = 0.0f;

                index += 3;

                //V2
                y = Y + (float) ((Radius + DxRadius)*Math.Sin(MathHelper.degreeToRad(angle + DxAngle)));
                x = X + (float) ((Radius + DxRadius)*Math.Cos(MathHelper.degreeToRad(angle + DxAngle)));

                vertexCoords[index] = x;
                vertexCoords[index + 1] = y;
                vertexCoords[index + 2] = 0.0f;

                index += 3;

                //V3
                y = Y + (float) ((Radius + DxRadius)*Math.Sin(MathHelper.degreeToRad(angle)));
                x = X + (float) ((Radius + DxRadius)*Math.Cos(MathHelper.degreeToRad(angle)));

                vertexCoords[index] = x;
                vertexCoords[index + 1] = y;
                vertexCoords[index + 2] = 0.0f;

                index += 3;
            }

            return vertexCoords;
        }

        protected override short[] GetDrawingOrder()
        {
            var numberRect = (int) ((EndAngle - StartAngle)/DxAngle);
            var drawOrder = new short[numberRect*6];
            var index = 0;

            for (var i = 0; i < numberRect; i++)
            {
                // { 0, 1, 2, 0, 2, 3 }
                drawOrder[index] = (short) (i*4);
                drawOrder[index + 1] = (short) (i*4 + 1);
                drawOrder[index + 2] = (short) (i*4 + 2);
                drawOrder[index + 3] = (short) (i*4);
                drawOrder[index + 4] = (short) (i*4 + 2);
                drawOrder[index + 5] = (short) (i*4 + 3);

                index += 6;
            }

            return drawOrder;
        }
    }
}