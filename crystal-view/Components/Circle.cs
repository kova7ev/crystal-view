namespace crystal_view.Components
{
    public class Circle : CircleSegment
    {
        public Circle(float x, float y, float radius, string color)
            : base(x, y, 0, 360, 0, radius, color)
        {
        }

        public Circle(float x, float y, float radius, float[] color)
            : base(x, y, 0, 360, 0, radius, color)
        {
        }
    }
}