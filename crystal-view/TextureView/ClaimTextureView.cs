using Javax.Microedition.Khronos.Egl;
using Javax.Microedition.Khronos.Opengles;
using Object = Java.Lang.Object;

namespace crystal_view.TextureView
{
    public class ClaimTextureView : Object, GLTextureView.IRenderer
    {
        private ClaimDiagram _claim = new ClaimDiagram();

        public void OnDrawFrame(IGL10 gl)
        {
            _claim.OnDrawFrame(gl);
        }

        public void OnSurfaceChanged(IGL10 gl, int width, int height)
        {
            _claim.OnSurfaceChanged(gl, width, height);
        }

        public void OnSurfaceCreated(IGL10 gl, EGLConfig config)
        {
            _claim.OnSurfaceCreated(gl, config);
        }

        public void OnSurfaceDestroyed(IGL10 p0)
        {
            
        }
    }
}