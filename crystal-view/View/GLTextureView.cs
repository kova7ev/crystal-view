//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Android.App;
//using Android.Content;
//using Android.Graphics;
//using Android.Opengl;
//using Android.OS;
//using Android.Runtime;
//using Android.Util;
//using Android.Views;
//using Android.Widget;
//using Java.Lang;
//using Javax.Microedition.Khronos.Egl;
//using Javax.Microedition.Khronos.Opengles;
//using OmcMobileClient.Android.Charts.View;
//using Exception = System.Exception;

//namespace OmcMobileClient.Android.Charts.View
//{
//     public enum GlesVersion {
//        /**
//         * OpenGL ES 1.0
//         */
//        OpenGles11,
//        OpenGles20
//    }

//    public class GLTextureView : TextureView, TextureView.ISurfaceTextureListener
//    {
//        private BidDiagram _view;
//        GlesVersion version = GlesVersion.OpenGles11;

//        public GLTextureView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
//        {

//        }

//        public GLTextureView(Context context) : base(context)
//        {
//        }

//        public GLTextureView(Context context, IAttributeSet attrs) : base(context, attrs)
//        {
//        }

//        private void Init()
//        {
//            _view = new BidDiagram();
//        }

//        int surfaceWidth;
//        int surfaceHeight;
//        bool isInitialized;

//        public void OnSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
//        {

//            surfaceWidth = width;
//            surfaceHeight = height;

//            if (!isInitialized())
//            {
//                eglManager = new EGLManager();

//                if (eglConfigChooser == null)
//                {
//                    // make default spec
//                    // RGBA8 hasDepth hasStencil
//                    eglConfigChooser = new DefaultEGLConfigChooser();
//                }

//                eglManager.initialize(eglConfigChooser, version);

//                if (version == GLESVersion.OpenGLES11)
//                {
//                    gl11 = eglManager.getGL11();
//                }

//                eglManager.resize(surface);

//                if (renderingThreadType != RenderingThreadType.BackgroundThread)
//                {
//                    // UIThread || request
//                    eglManager.bind();
//                    renderer.onSurfaceCreated(gl11, eglManager.getConfig());
//                    renderer.onSurfaceChanged(gl11, width, height);
//                    eglManager.unbind();
//                }
//            }
//            else {
//                eglManager.resize(surface);

//                if (renderingThreadType != RenderingThreadType.BackgroundThread)
//                {
//                    // UIThread || request
//                    eglManager.bind();
//                    renderer.onSurfaceChanged(gl11, width, height);
//                    eglManager.unbind();
//                }
//            }

//            initialized = true;
//            if (renderingThreadType == RenderingThreadType.BackgroundThread)
//            {
//                // background
//                backgroundThread = createRenderingThread();
//                backgroundThread.start();
//            }
//        }

//        public bool OnSurfaceTextureDestroyed(SurfaceTexture surface)
//        {
//            throw new NotImplementedException();
//        }

//        public void OnSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
//        {
//            throw new NotImplementedException();
//        }

//        public void OnSurfaceTextureUpdated(SurfaceTexture surface)
//        {
//            throw new NotImplementedException();
//        }
//    }
//}