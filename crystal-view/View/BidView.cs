using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Util;
using crystal_view.Charts;

namespace crystal_view.View
{
    public class BidView : BaseView
    {
        private BidDiagram _view;

        public BidView(Context context) : base(context)
        {
        }

        public BidView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public BidDiagram View
        {
            get
            {
                if (_view == null)
                {
                    _view = new BidDiagram();
                    //_view.OnDrawHandler += (sender, args) =>
                    //{
                    //    //RequestRender();
                    //    ((Activity) Context).RunOnUiThread(() => { SetBackgroundColor(Color.Transparent); });
                    //};
                }
                return _view;
            }
        }

        protected override IRenderer GetRenderer()
        {
            return View;
        }
    }
}