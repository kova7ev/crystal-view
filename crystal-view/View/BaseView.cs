using System;
using Android.Content;
using Android.Opengl;
using Android.Util;

namespace crystal_view.View
{
	public class BaseView : GLSurfaceView
    {
        private float _ratio = 0.64f;

        public BaseView(Context context) : base(context)
        {
            Init(context);
        }

        public BaseView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        private void Init(Context context)
        {
            // Create an OpenGL ES 2.0 context.
            SetEGLContextClientVersion(2);

            // Set the Renderer for drawing on the GLSurfaceView
            SetRenderer(GetRenderer());

            // Render the view only when there is a change in the drawing data
			RenderMode = Rendermode.WhenDirty;
        }

        protected virtual IRenderer GetRenderer()
        {
            throw new NotImplementedException();
        }

        //public void Render()
        //{
        //    RequestRender();
        //}

        //     protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        //     {
        //         base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
        //         var size = 0;
        //         var width = MeasuredWidth;
        //         var height = MeasuredHeight;

        //         if (width < height)
        //         {
        //             size = height;
        //         }
        //         else
        //         {
        //             size = width;
        //         }

        //var widgetHeight = height;
        //var widgetWidth = widgetHeight / _ratio;
        //SetMeasuredDimension((int)widgetWidth, (int)widgetHeight);
        //     }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
            var size = 0;
            var width = MeasuredWidth;
            var height = MeasuredHeight;

            if (width < height)
            {
                size = height;
            }
            else
            {
                size = width;
            }

            //var widgetHeight = height;
            //var widgetWidth = widgetHeight / _ratio;
            SetMeasuredDimension(size, (int)(size * _ratio));
        }
    }
}