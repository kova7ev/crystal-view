using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Util;
using crystal_view.Charts;

namespace crystal_view.View
{
    public class ClaimView : BaseView
    {
        private ClaimDiagram _view;

        public ClaimView(Context context) : base(context)
        {
        }

        public ClaimView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public ClaimDiagram View
        {
            get
            {
                if (_view == null)
                {
                    _view = new ClaimDiagram();
                    //_view.OnDrawHandler += (sender, args) =>
                    //{
                    //    //RequestRender();
                    //    ((Activity)Context).RunOnUiThread(() =>
                    //    {
                    //        SetBackgroundColor(Color.Transparent);
                    //    });
                    //};
                }
                return _view;
            }
        }

        protected override IRenderer GetRenderer()
        {
            return View;
        }
    }
}