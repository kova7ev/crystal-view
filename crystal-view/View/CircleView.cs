using Android.Opengl;
using crystal_view.Components;
using Javax.Microedition.Khronos.Opengles;
using EGLConfig = Javax.Microedition.Khronos.Egl.EGLConfig;
using Math = System.Math;
using Object = Java.Lang.Object;

namespace crystal_view.View
{
    public class CircleView : Object, GLSurfaceView.IRenderer
    {
        private static string TAG = "CircleView";
        //private Triangle mTriangle;
        private Circle mCircle;

        //private Text mText;
        //private Square   mSquare;

        // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
        private readonly float[] mMVPMatrix = new float[16];
        private readonly float[] mProjectionMatrix = new float[16];
        private readonly float[] mViewMatrix = new float[16];
        //private final float[] mRotationMatrix = new float[16];

        private float mAngle;
        private float mWidth;
        private float mHeight;
        private float mRatio;

        //public void OnSurfaceCreated(IGL10 gl, EGLConfig config)
        //{
        //    throw new NotImplementedException();
        //}

        public void OnSurfaceCreated(IGL10 gl, EGLConfig config)
        {
            //Enable blending
            GLES20.GlEnable(GLES20.GlBlend);
            GLES20.GlBlendFunc(GLES20.GlSrcAlpha, GLES20.GlOneMinusSrcAlpha);
            // Set the background frame color
            GLES20.GlClearColor(1.0f, 1.0f, 1.0f, 1.0f);

            //TextRenderer.initialize("Roboto/Roboto-BlackItalic.ttf", 20, new[] {0.4f, 0.43f, 0.45f, 1.0f});
            //SimpleText.OnSurfaceCreated(config);

            mCircle = new Circle(0, 0, 1, "#ff0000");
        }

        public void OnDrawFrame(IGL10 unused)
        {
            //float[] scratch = new float[16];

            // Draw background color
            GLES20.GlClear(GLES20.GlColorBufferBit | GLES20.GlDepthBufferBit);

            // Set the camera position (View matrix)
            Matrix.SetLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

            // Calculate the projection and view transformation
            Matrix.MultiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

            mCircle.Draw(mMVPMatrix);
        }

        public void OnSurfaceChanged(IGL10 unused, int width, int height)
        {
            // Adjust the viewport based on geometry changes,
            // such as screen rotation
            GLES20.GlViewport(0, 0, width, height);


            Line.OnSurfaceChanged(width, height);

            //float ratio = (float) width / height;
            mWidth = width;
            mHeight = height;
            mRatio = (float)Math.Min(width, height) / Math.Max(width, height);

            // this projection matrix is applied to object coordinates
            // in the onDrawFrame() method
            if (mWidth < mHeight)
            {
                Matrix.FrustumM(mProjectionMatrix, 0, -1, 1, -1 - mRatio, 1 + mRatio, 3, 7);
            }
            else
            {
                Matrix.FrustumM(mProjectionMatrix, 0, -1 - mRatio, 1 + mRatio, -1, 1, 3, 7);
            }
        }
    }
}