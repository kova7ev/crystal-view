using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Util;
using crystal_view.Charts;

namespace crystal_view.View
{
    public class IntegralView : BaseView
    {
        private IntegralDiagram _view;

        public IntegralView(Context context) : base(context)
        {
        }

        public IntegralView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public IntegralDiagram View
        {
            get
            {
                if (_view == null)
                {
                    _view = new IntegralDiagram();
                    //_view.OnDrawHandler += (sender, args) =>
                    //{
                    //    //RequestRender();
                    //    ((Activity)Context).RunOnUiThread(() =>
                    //    {
                    //        SetBackgroundColor(Color.Transparent);
                    //    });
                    //};
                }
                return _view;
            }
        }

        protected override IRenderer GetRenderer()
        {
            return View;
        }
    }
}