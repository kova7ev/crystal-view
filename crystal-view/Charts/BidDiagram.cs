using System;
using crystal_view.Base;
using crystal_view.Components;
using crystal_view.Utils;

namespace crystal_view.Charts
{
    public class BidDiagram : BaseDiagram
    {
        private CircleSegment _segment1;
        private CircleSegment _segment2;
        private CircleSegment _segment3;
        private SimpleText _textValue;

        private SimpleText _numberPending;
        private SimpleText _textPending;
        private SimpleText _numberInProgress;
        private SimpleText _textInProgress;
        private SimpleText _numberSolved;
        private SimpleText _textSolved;
        private SimpleText _textDescription;

        private Line _line;
        private float _percentageInProgress;
        private float _percentagePending;
        private float _percentageSolved;
        private int _total;

        public float PercentageInProgress
        {
            get { return _percentageInProgress; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_percentageInProgress = value;
				    Rebuild();
				}
            }
        }

        public float PercentagePending
        {
            get { return _percentagePending; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
                    _percentagePending = value;
                    Rebuild();
                }
            }
        }

        public float PercentageSolved
        {
            get { return _percentageSolved; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
                    _percentageSolved = value;
                    Rebuild();
                }
            }
        }

        public int Total
        {
            get { return _total; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
                    _total = value;
                    Rebuild();
                }
            }
        }

        private void Rebuild()
        {
            float angle1 = 0;
            float angle2 = 0;

            if (Total != 0)
            {
                angle1 = 1.0f * PercentageInProgress / 100 * 360;
                angle2 = 1.0f * (PercentageInProgress + PercentagePending) / 100 * 360;
            }

            var text = string.Format("�� {0}", Total);
            
            _segment1.StartAngle = 0;
            _segment1.EndAngle = angle1;

            _segment2.StartAngle = angle1;
            _segment2.EndAngle = angle2;

            _segment3.StartAngle = angle2;
            _segment3.EndAngle = 360;

            _textValue.Text = text;
            _numberInProgress.Text = $"{PercentageInProgress:0.0}%";
            _numberPending.Text = $"{PercentagePending:0.0}%";
            _numberSolved.Text = $"{PercentageSolved:0.0}%";
        }

        public BidDiagram()
        {
            float angle1 = 0;
            float angle2 = 0;

            if (Total != 0)
            {
                angle1 = 1.0f * PercentageInProgress / 100 * 360;
                angle2 = 1.0f * (PercentageInProgress + PercentagePending) / 100 * 360;
            }

            var text = string.Format("�� {0}", Total);
            var pieX = 0.8f;
            var pieY = -0.05f;

            _segment1 = new CircleSegment(pieX, pieY, 0, angle1, 0.5f, 0.2f, Colors.Pink);
            _segment2 = new CircleSegment(pieX, pieY, angle1, angle2, 0.5f, 0.2f, Colors.Violet);
            _segment3 = new CircleSegment(pieX, pieY, angle2, 360, 0.5f, 0.2f, Colors.Blue);

            Components.Add(_segment1);
            Components.Add(_segment2);
            Components.Add(_segment3);

            _textValue = new SimpleText(pieX, pieY, text, 26, Colors.Gray);
            _textDescription = new SimpleText(0.6f, 0.85f, "������ �� �������", 12, Colors.Gray);

            Components.Add(_textValue);
            Components.Add(_textDescription);

            _textInProgress = new SimpleText(-1.1f, 0.4f, "� ������", 12, Colors.Pink);
            _numberInProgress = new SimpleText(-1.28f, 0.55f, string.Format("{0:0.0}%", PercentageInProgress), 20,
                Colors.Pink);

            Components.Add(_textInProgress);
            Components.Add(_numberInProgress);

            _textPending = new SimpleText(-0.9f, -0.05f, "�� ������������", 12, Colors.Violet);
            _numberPending = new SimpleText(-1.28f, 0.10f, string.Format("{0:0.0}%", PercentagePending), 20, Colors.Violet);

            Components.Add(_textPending);
            Components.Add(_numberPending);

            _textSolved = new SimpleText(-1.1f, -0.5f, "������", 12, Colors.Blue);
            _numberSolved = new SimpleText(-1.28f, -0.35f, string.Format("{0:0.0}%", PercentageSolved), 20, Colors.Blue);

            Components.Add(_textSolved);
            Components.Add(_numberSolved);

            //_line = new Line(0.0f, -0.7f, 0.0f, 0.7f, 20, Colors.Gray);
            //Components.Add(_line);
        }

        protected override void OnCreate()
        {
            
        }

        protected override void OnDraw(float[] mvpMatrix)
        {
            Console.WriteLine("BidDiagram.OnDraw");
            foreach (var item in Components)
            {
                item.Draw(mvpMatrix);
            }

            //_segment1.Draw(mvpMatrix);
            //_segment2.Draw(mvpMatrix);
            //_segment3.Draw(mvpMatrix);
            //_textValue.Draw(mvpMatrix);
            //_line.Draw(mvpMatrix);

            //_textInProgress.Draw(mvpMatrix);
            //_textSolved.Draw(mvpMatrix);
            //_textPending.Draw(mvpMatrix);

            //_numberPending.Draw(mvpMatrix);
            //_numberInProgress.Draw(mvpMatrix);
            //_numberSolved.Draw(mvpMatrix);

            //_textDescription.Draw(mvpMatrix);

            FireOnDrawHandler ();
        }
    }
}