using System;
using crystal_view.Base;
using crystal_view.Components;
using crystal_view.Utils;

namespace crystal_view.Charts
{
    public class ClaimDiagram : BaseDiagram
    {
        private CircleSegment _segment1;
        private CircleSegment _segment2;
        private SimpleText _textValue;

        private SimpleText _numberInProgress;
        private SimpleText _textInProgress;
        private SimpleText _numberSolved;
        private SimpleText _textSolved;
        private SimpleText _textDescription;

        private Line _line;
        private int _numberInProgress1;
        private int _numberSolved1;
        private float _percentageSolved;

        public int NumberInProgress
        {
            get { return _numberInProgress1; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_numberInProgress1 = value;
                    Rebuild();
				}
            }
        }

        public int NumberSolved
        {
            get { return _numberSolved1; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_numberSolved1 = value;
                    Rebuild();
				}
            }
        }

        public float PercentageSolved
        {
            get { return _percentageSolved; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_percentageSolved = value;
                    Rebuild();
				}
            }
        }

        private void Rebuild()
        {
            var angle = 1.0f * PercentageSolved / 100 * 360;
            var text = string.Format("{0}%", (int)PercentageSolved);

            _segment1.StartAngle = 0;
            _segment1.EndAngle = angle;

            _segment2.StartAngle = angle;
            _segment2.EndAngle = 360;

            _textValue.Text = text;
            _numberInProgress.Text = string.Format("{0:0.0}", NumberInProgress);
            _numberSolved.Text = string.Format("{0:0.0}", NumberSolved);
        }

        public ClaimDiagram()
        {
            var angle = 1.0f * PercentageSolved / 100 * 360;

            var text = string.Format("{0}%", (int)PercentageSolved);

            var pieX = 0.8f;
            var pieY = -0.05f;

            _segment1 = new CircleSegment(pieX, pieY, 0, angle, 0.5f, 0.2f, Colors.Pink);
            _segment2 = new CircleSegment(pieX, pieY, angle, 360, 0.5f, 0.2f, Colors.Blue);

            Components.Add(_segment1);
            Components.Add(_segment2);

            _textValue = new SimpleText(pieX, pieY, text, 26, Colors.Gray);
            _textDescription = new SimpleText(0.6f, 0.85f, "������ �� ����������", 12, Colors.Gray);
            _textInProgress = new SimpleText(-0.9f, 0.2f, "��������� � ������", 12, Colors.Gray);
            _numberInProgress = new SimpleText(-1.28f, 0.35f, string.Format("{0:0.0}", NumberInProgress), 20, Colors.Gray);
            _textSolved = new SimpleText(-0.9f, -0.3f, "��������� ������", 12, Colors.Gray);
            _numberSolved = new SimpleText(-1.28f, -0.15f, string.Format("{0:0.0}", NumberSolved), 20, Colors.Gray);

            Components.Add(_textValue);
            Components.Add(_textDescription);
            Components.Add(_textInProgress);
            Components.Add(_numberInProgress);
            Components.Add(_textSolved);
            Components.Add(_numberSolved);

            //_line = new Line(0.0f, -0.7f, 0.0f, 0.7f, 20, new[] { 0.4f, 0.43f, 0.45f, 1.0f });
            //Components.Add(_line);
        }

        protected override void OnCreate()
        {
            
        }

        protected override void OnDraw(float[] mvpMatrix)
        {

            Console.WriteLine("ClaimDiagram.OnDraw");
            foreach (var item in Components)
            {
                item.Draw(mvpMatrix);
            }

			//_segment1.Draw(mvpMatrix);
   //         _segment2.Draw(mvpMatrix);
   //         _textValue.Draw(mvpMatrix);
   //         _line.Draw(mvpMatrix);
   //         _textInProgress.Draw(mvpMatrix);
   //         _textSolved.Draw(mvpMatrix);
   //         _numberInProgress.Draw(mvpMatrix);
   //         _numberSolved.Draw(mvpMatrix);
   //         _textDescription.Draw(mvpMatrix);

			FireOnDrawHandler ();
        }
    }
}