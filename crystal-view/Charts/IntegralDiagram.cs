using System;
using crystal_view.Base;
using crystal_view.Components;

namespace crystal_view.Charts
{
    public class IntegralDiagram : BaseDiagram
    {
        private SimpleText _textDescription;

        private Circle _circleLeft;
        private Circle _circleRight;
        private SimpleText _textValueLeft;
        private SimpleText _textValueRight;
        private SimpleText _textDescriptionLeft;
        private SimpleText _textDescriptionRightLine1;
        private SimpleText _textDescriptionRightLine2;

        private CircleSegment _segment1;
        private CircleSegment _segment2;
        private CircleSegment _segment3;
        private CircleSegment _segment4;
        private CircleSegment _segment5;
        private SimpleText _textValue;
        private float _period;
        private float _mark;
        private float _integralIndex;
        private float _valueA;
        private float _valueB;


        public float Period
        {
            get { return _period; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_period = value;
                    Rebuild();
				}
            }
        }

        public float Mark
        {
            get { return _mark; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_mark = value;
                    Rebuild();
				}
            }
        }

        public float IntegralIndex
        {
            get { return _integralIndex; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_integralIndex = value;
                    Rebuild();
				}
            }
        }

        public float ValueA
        {
            get { return _valueA; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_valueA = value;
                    Rebuild();
				}
            }
        }

        public float ValueB
        {
            get { return _valueB; }
            set
            {
                //OnCreate();
				if (!float.IsNaN (value)) {
					_valueB = value;
                    Rebuild();
				}
            }
        }

        //public float ValueC { get; set; }
        //public float ValueD { get; set; }

        public void Rebuild()
        {
            float dxAngle = 60;
            var angleStart = -90 - (180 - 2 * dxAngle) / 2;
            var angleEnd = 90 + (180 - 2 * dxAngle) / 2;
            var angleRange = angleEnd - angleStart;

            float angle1 = 1.0f * ValueA * angleRange / 100;
            float angle2 = 1.0f * ValueB * angleRange / 100;
            float angle3 = 1.0f * angleRange;

            var text = string.Format("{0:0.0}%", IntegralIndex);

            _textValueLeft.Text = string.Format("{0:0.0}", Period);
            _textValueRight.Text = string.Format("{0:0.0}", Mark);
            _textValue.Text = text;

            _segment1.StartAngle = angleStart;
            _segment1.EndAngle = angleStart + angle1;

            _segment2.StartAngle = angleStart + angle1;
            _segment2.EndAngle = angleStart + angle2;

            _segment3.StartAngle = angleStart + angle2;
            _segment3.EndAngle = angleEnd;

            _segment4.StartAngle = angleStart;
            _segment4.EndAngle = angleStart + angle3;

            _segment5.StartAngle = angleStart;
            _segment5.EndAngle = angleEnd;
        }

        public IntegralDiagram()
        {
            float dxAngle = 60;
            var angleStart = -90 - (180 - 2 * dxAngle) / 2;
            var angleEnd = 90 + (180 - 2 * dxAngle) / 2;
            var angleRange = angleEnd - angleStart;

            float angle1 = 0;
            float angle2 = 0;
            float angle3 = 0;

            angle1 = 1.0f * ValueA * angleRange / 100;
            angle2 = 1.0f * ValueB * angleRange / 100;
            angle3 = 1.0f * angleRange;


            var text = string.Format("{0:0.0}%", IntegralIndex);

            var pieX = 0f;
            var pieY = 0f;

            _textDescription = new SimpleText(0.4f, 0.85f, "�������� ������ �� �������", 12,
                new[] { 0.4f, 0.43f, 0.45f, 1.0f });

            Components.Add(_textDescription);

            _circleLeft = new Circle(-1.02f, 0, 0.25f, "#ffa553");
            _circleRight = new Circle(1.02f, 0, 0.25f, "#ffa553");

            Components.Add(_circleLeft);
            Components.Add(_circleRight);

            _textValueLeft = new SimpleText(1.02f, 0.0f, string.Format("{0:0.0}", Period), 18, "#FFFFFF");
            _textValueRight = new SimpleText(-1.02f, 0.0f, string.Format("{0:0.0}", Mark), 18, "#FFFFFF");
            _textValue = new SimpleText(pieX, pieY, text, 24, "#FFFFFF");

            Components.Add(_textValueLeft);
            Components.Add(_textValueRight);
            Components.Add(_textValue);

            _textDescriptionLeft = new SimpleText(1.02f, -0.45f, "���� %", 12, "#677074");
            _textDescriptionRightLine1 = new SimpleText(-1.02f, -0.40f, "�������", 12, "#677074");
            _textDescriptionRightLine2 = new SimpleText(-1.02f, -0.55f, "������", 12, "#677074");

            Components.Add(_textDescriptionLeft);
            Components.Add(_textDescriptionRightLine1);
            Components.Add(_textDescriptionRightLine2);

            _segment1 = new CircleSegment(pieX, pieY, angleStart, angleStart + angle1, 0.45f, 0.2f, "#f85c5c");
            _segment2 = new CircleSegment(pieX, pieY, angleStart + angle1, angleStart + angle2, 0.45f, 0.2f, "#ffa553");
            _segment3 = new CircleSegment(pieX, pieY, angleStart + angle2, angleEnd, 0.45f, 0.2f, "#00be6e");
            _segment4 = new CircleSegment(pieX, pieY, angleStart, angleStart + angle3, 0.65f, 0.015f, "#677074");
            _segment5 = new CircleSegment(pieX, pieY, angleStart, angleEnd, 0.665f, 0.008f, "#e1e1e1");

            Components.Add(_segment1);
            Components.Add(_segment2);
            Components.Add(_segment3);
            Components.Add(_segment4);
            Components.Add(_segment5);
        }

        protected override void OnCreate()
        {
            
        }

        protected override void OnDraw(float[] mvpMatrix)
        {
            Console.WriteLine("IntegralDiagram.OnDraw");
            foreach (var item in Components)
            {
                item.Draw(mvpMatrix);
            }

            //_circleLeft.Draw(mvpMatrix);
            //_circleRight.Draw(mvpMatrix);

            //_textDescription.Draw(mvpMatrix);
            //_textValueLeft.Draw(mvpMatrix);
            //_textValueRight.Draw(mvpMatrix);
            //_textDescriptionLeft.Draw(mvpMatrix);
            //_textDescriptionRightLine1.Draw(mvpMatrix);
            //_textDescriptionRightLine2.Draw(mvpMatrix);

            //_segment1.Draw(mvpMatrix);
            //_segment2.Draw(mvpMatrix);
            //_segment3.Draw(mvpMatrix);
            //_segment4.Draw(mvpMatrix);
            //_segment5.Draw(mvpMatrix);
            //_textValue.Draw(mvpMatrix);

            FireOnDrawHandler ();
        }
    }
}