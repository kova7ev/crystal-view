using System;

namespace crystal_view.Utils
{
    public class MathHelper
    {
        public static float degreeToRad(float degree)
        {
            return (float)(1.0f * Math.PI / 180) * degree;
        }
    }

}