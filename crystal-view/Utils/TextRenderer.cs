using System;
using System.Collections.Generic;
using System.Threading;
using Android.Graphics;

namespace crystal_view.Utils
{
    public class TextRenderer
    {
        public static Bitmap Bitmap { get; private set; }

        public static int AvgCharacterWidth { get; private set; }
        public static int AvgCharacterHeight { get; private set; }

        public static int MaxCharacterWidth { get; private set; }
        public static int MaxCharacterHeight { get; private set; }
        
        private static string Keys;
        private static Dictionary<string, Rect> Position;
        private static SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        static TextRenderer()
        {
            Keys = "01234567890.,% �������������������������������������Ũ��������������������������";
            Position = new Dictionary<string, Rect>();
        }

		public static async void Initialize(string fontType, float fontSize, float[] color)
        {
            await _semaphore.WaitAsync();

            try
            {
                if (Bitmap == null)
                {
                    var resources = ApplicationContext.Context.Resources;
                    var density = resources.DisplayMetrics.Density;

                    var asset = ApplicationContext.Context.Assets;
                    var tf = Typeface.CreateFromAsset(asset, string.Format("fonts/{0}", fontType));

                    //for rect
                    var paintStrokeOnly = new Paint(PaintFlags.AntiAlias)
                    {
                        Color = Color.Argb(255, 0, 0, 0),
                        StrokeWidth = 2
                    };
                    paintStrokeOnly.SetStyle(Paint.Style.Stroke);

                    // new antialised Paint
                    var paint = new Paint(PaintFlags.AntiAlias)
                    {
                        Color = Color.Argb(255, 0, 0, 0),
                        TextSize = (int)(fontSize * density)
                    };
                    //paint.SetTypeface(tf);

                    // text size in pixels
                    // using scale for case if texture will be upscaled
                    // text shadow
                    paint.SetShadowLayer(1f, 0f, 1f, Color.White);

                    for (var i = 0; i < Keys.Length; i++)
                    {
                        // draw text to the Canvas center
                        var text = Keys.Substring(i, 1);
                        var bounds = new Rect();
                        paint.GetTextBounds(text, 0, text.Length, bounds);
                        var width = Math.Abs(bounds.Left) + Math.Abs(bounds.Right);
                        var height = Math.Abs(bounds.Bottom) + Math.Abs(bounds.Top);

                        AvgCharacterWidth += width;
                        AvgCharacterHeight += height;

                        MaxCharacterWidth = Math.Max(MaxCharacterWidth, width);
                        MaxCharacterHeight = Math.Max(MaxCharacterHeight, height);
                    }

                    AvgCharacterWidth /= Keys.Length;
                    AvgCharacterHeight /= Keys.Length;

                    var maxCharacterSize = Math.Max(MaxCharacterWidth, MaxCharacterHeight);

                    var side = (int) Math.Sqrt(Keys.Length) + 1;
                    var size = side * maxCharacterSize;

                    //var w = 2;
                    //while (w < MaxCharacterWidth * Keys.Length)
                    //{
                    //    w *= 2;
                    //}

                    //var h = 2;
                    //while (h < MaxCharacterHeight)
                    //{
                    //    h *= 2;
                    //}

                    var actualSize = 2;
                    while (actualSize < size)
                    {
                        actualSize *= 2;
                    }


                    Console.WriteLine(string.Format("ACTUAL TEXTURE SIZE: {0}x{0}", actualSize));
                    Bitmap = Bitmap.CreateBitmap(actualSize, actualSize, Bitmap.Config.Argb8888);
                    Bitmap.EraseColor(Color.Argb(0, 0, 0, 0));
                    //Bitmap.EraseColor(Color.Argb(255, 255, 255, 255));
                    // get a canvas to paint over the bitmap
                    var canvas = new Canvas(Bitmap);

                    //var currentX = 0;
                    for (var i = 0; i < Keys.Length; i++)
                    {
                        var text = Keys.Substring(i, 1);

                        var bounds = new Rect();
                        paint.GetTextBounds(text, 0, text.Length, bounds);

                        var width = Math.Abs(bounds.Left) + Math.Abs(bounds.Right);
                        var height = Math.Abs(bounds.Bottom) + Math.Abs(bounds.Top);

						Console.WriteLine(string.Format("{0}_size: {1}x{2}", text, width, height));
						Console.WriteLine(string.Format("{0}_rect: {1},{2},{3},{4}", text, bounds.Left, bounds.Bottom, bounds.Right, bounds.Top));

                        var row = i / side + 1;
                        var column = i - (row-1) * side;

                        //TODO: Prev implementation
                        //var x = column * maxCharacterSize + width;
                        //var y = row * maxCharacterSize + height;

                        //var convexHullX = x;
                        //var convexHullY = y - height;

                        //canvas.DrawText(text, x, y, paint);
                        ////canvas.DrawRect(convexHullX, convexHullY, convexHullX+width, convexHullY+height, paintStrokeOnly);
                        //canvas.DrawRect(column * maxCharacterSize, row * maxCharacterSize, column * maxCharacterSize + maxCharacterSize, row * maxCharacterSize + maxCharacterSize, paintStrokeOnly);

                        //TODO: Rectagle position in center each cell
                        //var x = column * maxCharacterSize;
                        //var y = row * maxCharacterSize;

                        //var dx = maxCharacterSize - width;
                        //var dy = maxCharacterSize - height;

                        //canvas.DrawText(text, x + dx/2, y - dy/2, paint);
                        ////canvas.DrawRect(convexHullX, convexHullY, convexHullX+width, convexHullY+height, paintStrokeOnly);
                        //canvas.DrawRect(x, y, x + maxCharacterSize, y + maxCharacterSize, paintStrokeOnly);

                        var dx = maxCharacterSize - width;
                        var dy = maxCharacterSize - height;

						var x = (int)(column * maxCharacterSize + 0.5f * dx);
						var y = (int)(row * maxCharacterSize - 0.5f * dy);


                        canvas.DrawText(text, x, y, paint);
                        //canvas.DrawRect(convexHullX, convexHullY, convexHullX+width, convexHullY+height, paintStrokeOnly);
//						if (row == 1){
//							var x_rect = (int)(column * maxCharacterSize);
//							var y_rect = (int)(row * maxCharacterSize);
//
////							canvas.DrawRect(x, y + height / 2 - 0.5f * dy, x + width, (y + height / 2 - 0.5f * dy) + height, paintStrokeOnly);
//							canvas.DrawRect(x_rect, y_rect, x_rect + maxCharacterSize, y_rect + maxCharacterSize, paintStrokeOnly);
//						}

						var x_rect = (int)(column * maxCharacterSize + 0.5f * dx);
						var y_rect = (int)(row * maxCharacterSize + 0.5f * dy);
						//if (row == 1)
						{
							

							//							canvas.DrawRect(x, y + height / 2 - 0.5f * dy, x + width, (y + height / 2 - 0.5f * dy) + height, paintStrokeOnly);
							//canvas.DrawRect(x_rect, y_rect, x_rect + width, y_rect + height, paintStrokeOnly);
						}

						Position[text] = new Rect(x_rect, y_rect- maxCharacterSize - 5, x_rect + width, y_rect + height - maxCharacterSize + 10);
                        //currentX += (int)(width * 1.5f);
                    }

                    //TODO: For debug purpose only
                    //FileHelper.WriteFile("atlas.bmp", ImageProcessingHelper.ImageToByteArray(Bitmap));
                }
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public static Rect GetTextBoundingBox(string text)
        {
            var width = 0;
            var height = 0;
            for (var i = 0; i < text.Length; i++)
            {
               var c = text.Substring(i, 1);
                var rect = GetTextureCoords(c);
				width  += rect.Right - rect.Left;
                height += rect.Bottom - rect.Top;
            }

            return new Rect(0, height, width, 0);
        }

        public static Rect GetTextureCoords(string text)
        {
            if (text == null || text.Length != 1 || !Keys.Contains(text))
            {
                return null;
            }

            return Position[text];
        }
    }
}