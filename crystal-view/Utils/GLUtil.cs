using System;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Opengl;
using Java.Lang;
using Java.Nio;

namespace crystal_view.Utils
{
    public class GLUtil
    {
        public static int BYTES_PER_FLOAT = 4;

        public static int loadShader(int type, string shaderCode)
        {
            // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
            // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
            var shaderHandle = GLES20.GlCreateShader(type);

            // add the source code to the shader and compile it
            GLES20.GlShaderSource(shaderHandle, shaderCode);
            GLES20.GlCompileShader(shaderHandle);
            checkGlError("glCompileShader");
            return shaderHandle;
        }

        public static int createAndLinkProgram(int vertexShaderHandle, int fragShaderHandle,
            string[] attributes)
        {
            var programHandle = GLES20.GlCreateProgram();
            checkGlError("glCreateProgram");
            GLES20.GlAttachShader(programHandle, vertexShaderHandle);
            GLES20.GlAttachShader(programHandle, fragShaderHandle);
            if (attributes != null)
            {
                var size = attributes.Length;
                for (var i = 0; i < size; i++)
                {
                    GLES20.GlBindAttribLocation(programHandle, i, attributes[i]);
                }
            }
            GLES20.GlLinkProgram(programHandle);
            checkGlError("glLinkProgram");
            GLES20.GlDeleteShader(vertexShaderHandle);
            GLES20.GlDeleteShader(fragShaderHandle);
            return programHandle;
        }

        public static Bitmap getResourceById(Context context, int id)
        {
            var drawable = context.Resources.GetDrawable(id);
            return ((BitmapDrawable) drawable).Bitmap;
        }


        public static int LoadTexture(Bitmap bitmap)
        {
            var textureHandle = new int[1];

            GLES20.GlGenTextures(1, textureHandle, 0);
            checkGlError("glGenTextures");

            if (textureHandle[0] != 0)
            {
                // Bind to the texture in OpenGL
                GLES20.GlBindTexture(GLES20.GlTexture2d, textureHandle[0]);

                // Set filtering
                GLES20.GlTexParameteri(GLES20.GlTexture2d, GLES20.GlTextureWrapS,
                    GLES20.GlClampToEdge);
                GLES20.GlTexParameteri(GLES20.GlTexture2d, GLES20.GlTextureWrapT,
                    GLES20.GlClampToEdge);
                GLES20.GlTexParameteri(GLES20.GlTexture2d, GLES20.GlTextureMinFilter,
                    GLES20.GlLinear);
                GLES20.GlTexParameteri(GLES20.GlTexture2d, GLES20.GlTextureMagFilter,
                    GLES20.GlLinear);

                // Load the bitmap into the bound texture.
                GLUtils.TexImage2D(GLES20.GlTexture2d, 0, bitmap, 0);
                checkGlError("texImage2D");
            }

            //if (textureHandle[0] == 0)
            //{
            //    Log.e(TAG, "Error loading texture (empty texture handle)");
            //    if (BuildConfig.DEBUG)
            //    {
            //        throw new RuntimeException("Error loading texture (empty texture handle).");
            //    }
            //}
			if (textureHandle [0] == 0) {
				Console.WriteLine ("Error loading texture (empty texture handle)");
				throw new RuntimeException ("Error loading texture (empty texture handle).");
			}

            return textureHandle[0];
        }

        public static void checkGlError(string glOperation)
        {
            int error;
            while ((error = GLES20.GlGetError()) != GLES20.GlNoError)
            {
                //Log.e(TAG, glOperation + ": glError " + error);
                //if (BuildConfig.DEBUG)
                //{
                //    throw new RuntimeException(glOperation + ": glError " + error);
                //}
                Console.WriteLine(glOperation + ": glError " + error);
                throw new RuntimeException(glOperation + ": glError " + error);
            }
        }

        public static FloatBuffer asFloatBuffer(float[] array)
        {
            var buffer = newFloatBuffer(array.Length);
            buffer.Put(array);
            buffer.Position(0);
            return buffer;
        }

        public static FloatBuffer newFloatBuffer(int size)
        {
            var buffer = ByteBuffer.AllocateDirect(size*BYTES_PER_FLOAT)
                .Order(ByteOrder.NativeOrder())
                .AsFloatBuffer();
            buffer.Position(0);
            return buffer;
        }
    }
}