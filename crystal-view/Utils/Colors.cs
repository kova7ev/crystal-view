namespace crystal_view.Utils
{
    public class Colors
    {
        public static string Gray
        {
            get { return "#677074"; }
        }

        public static string Pink
        {
            get { return "#d2527f"; }
        }

        public static string Violet
        {
            get { return "#be90d4"; }
        }

        public static string Blue
        {
            get { return "#22a7f0"; }
        }
    }
}