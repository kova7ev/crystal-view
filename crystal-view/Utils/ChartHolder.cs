using System;
using Android.Content;
using Android.Opengl;
using Android.Util;
using crystal_view.Charts;

namespace crystal_view.Utils
{
    public class ChartHolder : GLSurfaceView
    {
        private IRenderer mRenderer;


        public ChartHolder(Context context) : base(context)
        {
            Init(context);
        }

        public ChartHolder(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
			Console.WriteLine(string.Format("SCREEN: {0}x{1}", widthMeasureSpec, heightMeasureSpec));
            int size = 0;
            int width = MeasuredWidth;
            int height = MeasuredHeight;

            if (width < height)
            {
                size = height;
            }
            else {
                size = width;
            }
            SetMeasuredDimension(size, (int)(size * 0.64f));
        }

        private void Init(Context context)
        {
            // Create an OpenGL ES 2.0 context.
            SetEGLContextClientVersion(2);

            // Set the Renderer for drawing on the GLSurfaceView
			mRenderer = new IntegralDiagram();
            SetRenderer(mRenderer);

            // Render the view only when there is a change in the drawing data
			RenderMode = Rendermode.WhenDirty;
        }

//		public void RequestRender(){
//			RequestRender ();
//		}
    }
}