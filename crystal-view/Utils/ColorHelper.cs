using System;
using System.Globalization;

namespace crystal_view.Utils
{
    public class ColorHelper
    {
        public static float[] hexToColor(String hex)
        {
            if (hex == null || hex.Length != 7 || !hex.Substring(0, 1).Equals("#"))
            {
                return null;
            }

            float r = 1.0f * int.Parse(hex.Substring(1, 2), NumberStyles.HexNumber) / 255;
            float g = 1.0f * int.Parse(hex.Substring(3, 2), NumberStyles.HexNumber) / 255;
            float b = 1.0f * int.Parse(hex.Substring(5, 2), NumberStyles.HexNumber) / 255;
            return new float[] { r, g, b, 1.0f };
        }
    }

}