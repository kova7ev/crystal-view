namespace crystal_view.Utils
{
    public static class Shaders
    {
        private static readonly string _simpleVertexShader =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
            "attribute vec4 vPosition;" +
            "void main() {" +
            // the matrix must be included as a modifier of gl_Position
            // Note that the uMVPMatrix factor *must be first* in order
            // for the matrix multiplication product to be correct.
            "  gl_Position = uMVPMatrix * vPosition;" +
            "}";

        private static readonly string _simpleFragmentShader = "precision mediump float;"
                                                               + "uniform vec4 vColor;" + "void main() {"
                                                               + "  gl_FragColor = vColor;" + "}";

        private static string _vertexShader = ""
                                              +
                                              // This matrix member variable provides a hook to manipulate
                                              // the coordinates of the objects that use this vertex shader
                                              "uniform mat4 uMVPMatrix;" + "attribute vec4 aPosition;"
                                              + "attribute vec2 aTexCoords;" + "varying vec2 vTexCoords;"
                                              + "void main(){" + "  vTexCoords = aTexCoords;"
                                              + "  gl_Position = uMVPMatrix * aPosition;" + "}";

        private static string _fragmentShader = ""
                                                + "precision mediump float;" + "uniform sampler2D uTexture;"
                                                + "uniform float uAlpha;" + "varying vec2 vTexCoords;"
                                                + "void main(){"
                                                + "  gl_FragColor = texture2D(uTexture, vTexCoords);"
            // + "  gl_FragColor.a = uAlpha;"
                                                + "}";

        public static string SimpleVertexShader
        {
            get { return _simpleVertexShader; }
        }

        public static string SimpleFragmentShader
        {
            get { return _simpleFragmentShader; }
        }

        public static string VertexShader
        {
            get { return _vertexShader; }
            set { _vertexShader = value; }
        }

        public static string FragmentShader
        {
            get { return _fragmentShader; }
            set { _fragmentShader = value; }
        }
    }
}