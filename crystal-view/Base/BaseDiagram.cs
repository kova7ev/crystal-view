using System;
using System.Collections.Generic;
using Android.Opengl;
using crystal_view.Components;
using crystal_view.Utils;
using Javax.Microedition.Khronos.Opengles;
using EGLConfig = Javax.Microedition.Khronos.Egl.EGLConfig;
using Math = System.Math;
using Object = Java.Lang.Object;

namespace crystal_view.Base
{
    public class BaseDiagram : Object, GLSurfaceView.IRenderer
    {
        // _mvpMatrix is an abbreviation for "Model View Projection Matrix"
        private readonly float[] _mvpMatrix = new float[16];
        private readonly float[] _projectionMatrix = new float[16];
        private readonly float[] _viewMatrix = new float[16];
        private float _height;
        private float _ratio;
        private float _width;

        protected List<Component> Components;

        public BaseDiagram()
        {
            Components = new List<Component>();
        } 

        public void OnSurfaceCreated(IGL10 unused, EGLConfig config)
        {
            //Enable blending
            GLES20.GlEnable(GLES20.GlBlend);
            GLES20.GlBlendFunc(GLES20.GlSrcAlpha, GLES20.GlOneMinusSrcAlpha);
            // Set the background frame color
            GLES20.GlClearColor(1.0f, 1.0f, 1.0f, 1.0f);

            TextRenderer.Initialize("Roboto/Roboto-BlackItalic.ttf", 20, new[] {0.4f, 0.43f, 0.45f, 1.0f});

            foreach (var item in Components)
            {
                item.OnSurfaceCreated(config);
            }

            OnCreate();
        }


        public void OnDrawFrame(IGL10 unused)
        {
            // Draw background color
            GLES20.GlClear(GLES20.GlColorBufferBit | GLES20.GlDepthBufferBit);

            // Set the camera position (View matrix)
            Matrix.SetLookAtM(_viewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

            // Calculate the projection and view transformation
            Matrix.MultiplyMM(_mvpMatrix, 0, _projectionMatrix, 0, _viewMatrix, 0);

            //OnCreate();
            OnDraw(_mvpMatrix);
        }

        public void OnSurfaceChanged(IGL10 unused, int width, int height)
        {
            foreach (var item in Components)
            {
                item.OnSurfaceChanged(unused, width, height);
            }

            // Adjust the viewport based on geometry changes,
            // such as screen rotation
            GLES20.GlViewport(0, 0, width, height);

            //TODO: Initialize on upper level
            Line.OnSurfaceChanged(width, height);

            _width = width;
            _height = height;
            _ratio = (float) Math.Min(width, height)/Math.Max(width, height);

            // this projection matrix is applied to object coordinates
            // in the onDrawFrame() method
            if (_width < _height)
            {
                Matrix.FrustumM(_projectionMatrix, 0, -1, 1, -1 - _ratio, 1 + _ratio, 3, 7);
            }
            else
            {
                Matrix.FrustumM(_projectionMatrix, 0, -1 - _ratio, 1 + _ratio, -1, 1, 3, 7);
            }
        }

        //public event OnDrawHandler OnDrawHandler;

        protected virtual void OnCreate()
        {
            throw new NotImplementedException();
        }

        protected virtual void OnDraw(float[] mvpMatrix)
        {
            throw new NotImplementedException();
        }

        protected void FireOnDrawHandler()
        {
            //if (OnDrawHandler != null)
            //{
            //    OnDrawHandler(this, EventArgs.Empty);
            //}
        }
    }
}